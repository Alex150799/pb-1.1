package Dz;

import java.util.Scanner;

public class Zad6 {
    public static void main(String[] args) {
        System.out.println("Введите число: ");
        Scanner x = new Scanner(System.in);
        int y = x.nextInt();

        if ((y < 0) && (y%2 == 0)){
            System.out.println("Отрицательное, чётное");
        }
        if ((y < 0) && (y%2 != 0)){
            System.out.println("Отрицательное, нечётное");
        }
        if ((y > 0) && (y%2 == 0)){
            System.out.println("Положительное, чётное");
        }
        if ((y > 0) && (y%2 != 0)){
            System.out.println("Положительное, нечётное");
        }
        if (y ==0){
            System.out.println("Ноль");
        }
    }
}
