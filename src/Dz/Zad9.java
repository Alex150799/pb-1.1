package Dz;
public class Zad9 {
    public static void main(String[] args) {
        int[] s = {15, 10, 51, -6, -5, 3, -10, -34, 0, 32, 56, -12, 24, -52};
        for (int i = 0, j = s.length - 1; i < j; ++i, --j) {
            int k = s[i];
            s[i] = s[j];
            s[j] = k;
        }
        for (int i : s)
            System.out.print(i + " ");
        System.out.println();
    }
}
