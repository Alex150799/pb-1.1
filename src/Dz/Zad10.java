package Dz;
public class Zad10 {
    public static void main(String[] args) {
        int[] s = {15, 10, 0, -6, -5, 3, 0, -34, 0, 32, 56, 0, 24, -52};
        int a = 0;
        int x = s.length - 1;

        for (int i = x - 1; i >= 0; i--) {
            if (s[i] == 0) {
                s[i] = s[x - a];
                s[x - a] = 0;
                a++;
            }
        }
        for (int i : s) {
            System.out.print(i + " ");
        }
    }
}
